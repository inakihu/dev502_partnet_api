import com.sforce.soap.partner.PartnerConnection;
	import com.sforce.soap.partner.sobject.*;
	import com.sforce.soap.partner.*;
	import com.sforce.ws.ConnectorConfig;
	import com.sforce.ws.ConnectionException;
	import com.sforce.soap.partner.Error;
	import java.io.FileNotFoundException;
	import java.io.IOException;
	import java.io.InputStreamReader;
	import java.io.BufferedReader;
	import java.util.*;
	import java.util.ArrayList;

	
public class StartWithPartner {
	PartnerConnection partnerConnection = null;
	
	
	private static BufferedReader reader =
	        new BufferedReader(new InputStreamReader(System.in));
	
	public static void main(String[] args) {
		StartWithPartner sample = new StartWithPartner();
        if (sample.login()) {
            // Add calls to the methods in this class.
        	
        	String idContact = sample.createContact();
        	
            // For example:
            sample.queryContacts();
            
            sample.updateContact(idContact);
            
            sample.queryContacts();
        }

	}
	
	
    private String getUserInput(String prompt) {
        String result = "";
        try {
          System.out.print(prompt);
          result = reader.readLine();
        } catch (IOException ioe) {
          ioe.printStackTrace();
        }
        return result;
    }
    
    private boolean login() {
        boolean success = false;
        //String username = getUserInput("Enter username: ");
        //String password = getUserInput("Enter password: ");
        String username = "dev502.sso1@gmail.com";
        String password = "Cloud20156vcLYTtW8wKUaYyGmRx4Hh2O";
        //String authEndPoint = getUserInput("Enter auth end point: ");
        String authEndPoint = "https://login.salesforce.com/services/Soap/u/35.0";

        try {
          ConnectorConfig config = new ConnectorConfig();
          config.setUsername(username);
          config.setPassword(password);
          
          config.setAuthEndpoint(authEndPoint);
          config.setTraceFile("traceLogs.txt");
          config.setTraceMessage(true);
          config.setPrettyPrintXml(true);

          partnerConnection = new PartnerConnection(config);          

          success = true;
        } catch (ConnectionException ce) {
          ce.printStackTrace();
        } catch (FileNotFoundException fnfe) {
          fnfe.printStackTrace();
        }

        return success;
      }
    
    
    public void queryContacts() {    
        try {
            // Set query batch size
            partnerConnection.setQueryOptions(200);
            
            // SOQL query to use 
            String soqlQuery = "SELECT FirstName, LastName, Phone FROM Contact";
            // Make the query call and get the query results
            QueryResult qr = partnerConnection.query(soqlQuery);
            
            boolean done = false;
            int loopCount = 0;
            // Loop through the batches of returned results
            while (!done) {
                System.out.println("Records in results set " + loopCount++
                        + " - ");
                SObject[] records = qr.getRecords();
                // Process the query results
                for (int i = 0; i < records.length; i++) {
                    SObject contact = records[i];
                    Object firstName = contact.getField("FirstName");
                    Object lastName = contact.getField("LastName");
                    Object phone = contact.getField("Phone");
                    if (firstName == null) {
                        System.out.print("Contact " + (i + 1) + 
                                ": " + lastName
                        );
                    } else {
                        System.out.print("Contact " + (i + 1) + ": " + 
                                firstName + " " + lastName);
                    }
                    if (phone == null) {
                    	System.out.println("");
                    }else{
                    	System.out.println(" - " + phone);
                    }
                }
                if (qr.isDone()) {
                    done = true;
                } else {
                	System.out.println("\n------ Query more.");
                    qr = partnerConnection.queryMore(qr.getQueryLocator());
                }
            }
        } catch(ConnectionException ce) {
            ce.printStackTrace();
        }
        System.out.println("\nQuery execution completed.");         
    }
    
    public String createContact() {
        String result = null;
        try {
            // Create a new sObject of type Contact
               // and fill out its fields.
            SObject contact = new SObject();
            contact.setType("Contact");
            contact.setField("FirstName", getUserInput("FirstName: "));
            contact.setField("LastName", getUserInput("LastName: "));
            //contact.setField("Salutation", getUserInput("salutation: "));
            contact.setField("Phone", getUserInput("Phone: "));
            //contact.setField("Title", getUserInput("FirstName: "));
        
            // Add this sObject to an array 
            SObject[] contacts = new SObject[1];
            contacts[0] = contact;
            // Make a create call and pass it the array of sObjects
            SaveResult[] results = partnerConnection.create(contacts);
        
            // Iterate through the results list
            // and write the ID of the new sObject
            // or the errors if the object creation failed.
            // In this case, we only have one result
            // since we created one contact.
            for (int j = 0; j < results.length; j++) {
                if (results[j].isSuccess()) {
                    result = results[j].getId();
                    System.out.println(
                        "\nA contact was created with an ID of: " + result
                    );
                 } else {
                    // There were errors during the create call,
                    // go through the errors array and write
                    // them to the console
                    for (int i = 0; i < results[j].getErrors().length; i++) {
                        Error err = results[j].getErrors()[i];
                        System.out.println("Errors were found on item " + j);
                        System.out.println("Error code: " + 
                            err.getStatusCode().toString());
                        System.out.println("Error message: " + err.getMessage());
                    }
                 }
            }
        } catch (ConnectionException ce) {
            ce.printStackTrace();
        }
        return result;
    }
    
    
    public void updateContact(String id) {
    	  try {
    		  
    		  System.out.println("Updating Contact " + id);
    	      // Create an sObject of type contact
    	      SObject updateContact = new SObject();
    	      updateContact.setType("Contact");
    	      
    	      // Set the ID of the contact to update
    	      updateContact.setId(id);
    	      // Set the Phone field with a new value
    	      updateContact.setField("FirstName", getUserInput("FirstName: "));
    	      updateContact.setField("LastName", getUserInput("LastName: "));
              //contact.setField("Salutation", getUserInput("salutation: "));
    	      String phone = getUserInput("Phone: ");
    	      
    	      if("".equals(phone) || phone==null){
    	    	  System.out.println("Phone empty." + id);
    	    	  updateContact.setFieldsToNull(new String[] {"Phone"});
    	      }else{
    	    	  System.out.println("Phone=" + phone) ;
    	    	  updateContact.setField("Phone", phone);
    	      }
              

    	      // Create another contact that will cause an error
    	      // because it has an invalid ID.
    	      SObject errorContact = new SObject();
    	      errorContact.setType("Contact");
    	      // Set an invalid ID on purpose
    	      errorContact.setId("SLFKJLFKJ");      
    	      // Set the value of LastName to null
    	      errorContact.setFieldsToNull(new String[] {"LastName"});

    	      // Make the update call by passing an array containing
    	      // the two objects. 
    	      SaveResult[] saveResults = partnerConnection.update(
    	         new SObject[] {updateContact, errorContact}
    	      );
    	      // Iterate through the results and write the ID of 
    	      // the updated contacts to the console, in this case one contact.
    	      // If the result is not successful, write the errors
    	      // to the console. In this case, one item failed to update.
    	      for (int j = 0; j < saveResults.length; j++) {
    	          System.out.println("\nItem: " + j);
    	          if (saveResults[j].isSuccess()) {
    	              System.out.println("Contact with an ID of " +
    	                      saveResults[j].getId() + " was updated.");
    	          }
    	          else {                        
    	            // There were errors during the update call,
    	            // go through the errors array and write
    	            // them to the console.
    	            for (int i = 0; i < saveResults[j].getErrors().length; i++) {
    	              Error err = saveResults[j].getErrors()[i];
    	              System.out.println("Errors were found on item " + j);
    	              System.out.println("Error code: " + 
    	                  err.getStatusCode().toString());
    	              System.out.println("Error message: " + err.getMessage());
    	            }
    	          }      
    	      }      
    	  } catch (ConnectionException ce) {
    	      ce.printStackTrace();
    	  }
    	}
    
}
